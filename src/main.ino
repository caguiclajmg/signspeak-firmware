//#define SS_ENABLE_SLEEP

#define SS_ENABLE_SWBT

#define MUX_HACK

#define I2CDEV_IMPLEMENTATION 4
#include <I2Cdev.h>
#include <MPU6050_6Axis_MotionApps20.h>

#include <SoftwareSerial.h>

#include <avr/power.h>
#include <avr/sleep.h>
#include <avr/wdt.h>

template<typename T, size_t M> class Filter {
public:
  void put(T value) {
    _index = (_index >= M) ? 0 : (_index + 1);
    _values_raw[_index] = value;
  }

  T get() const {
    return _values_filtered[M - 1];
  }

  T get(size_t index) const {
    return _values_filtered[index];
  }

protected:
  T _values_raw[M];
  T _values_filtered[M];

  Filter() { }

private:
  size_t _index = 0;
};

template<typename T, size_t M> class HighPassFilter : public Filter<T, M> {
public:
  HighPassFilter(float alpha) : _alpha(alpha) { }

  void compute() {
    for(size_t i = 1; i < M; ++i) _values_filtered[i] = _alpha * (_values_filtered[i - 1] + _values_raw[i] - _values_raw[i - 1]);
  }

private:
  float _alpha = 0.f;

  using Filter<T, M>::_values_raw;
  using Filter<T, M>::_values_filtered;
};

template<typename T, size_t M> class LowPassFilter : public Filter<T, M> {
public:
  LowPassFilter(float alpha) : _alpha(alpha) { }

  void compute() {
    for(size_t i = 1; i < M; ++i) _values_filtered[i] = _values_filtered[i - 1] + (_alpha * (_values_raw[i] - _values_filtered[i - 1]));
  }

private:
  float _alpha = 0.f;

  using Filter<T, M>::_values_raw;
  using Filter<T, M>::_values_filtered;
};

template<typename T, size_t M> class CumulativeMovingAverage : public Filter<T, M> {
public:
  CumulativeMovingAverage() { }

  void compute() {
    for(size_t i = 0; i < M; ++i) {
      _values_filtered[i] = static_cast<T>(0);

      for(size_t j = 0; j <= i; ++j) _values_filtered[i] += _values_raw[j];
      _values_filtered[i] /= i + 1;
    }
  }

private:
  using Filter<T, M>::_values_raw;
  using Filter<T, M>::_values_filtered;
};

constexpr int BATTERY_PIN_SENSE = A0;

constexpr int BLUETOOTH_PIN_STATE = 2;
constexpr int BLUETOOTH_PIN_RX = 4;
constexpr int BLUETOOTH_PIN_TX = 5;
constexpr int BLUETOOTH_PIN_EN = 6;
constexpr int BLUETOOTH_BAUD = 9600;

constexpr int MULTIPLEXER_PIN_SIG = A1;
constexpr int MULTIPLEXER_PIN_S3 = 7;
constexpr int MULTIPLEXER_PIN_S2 = 8;
constexpr int MULTIPLEXER_PIN_S1 = 9;
constexpr int MULTIPLEXER_PIN_S0 = 10;
constexpr int MULTIPLEXER_PIN_EN = 11;

constexpr int FLEXSENSOR_COUNT = 10;
constexpr int FLEXSENSOR_SAMPLES = 16;

constexpr int MPU_PIN_INTERRUPT = 3;

#ifdef SS_ENABLE_SWBT
SoftwareSerial _bluetooth(BLUETOOTH_PIN_RX, BLUETOOTH_PIN_TX);
#else
HardwareSerial _bluetooth = Serial;
#endif

using FlexSensorFilter = CumulativeMovingAverage<int, FLEXSENSOR_SAMPLES>;
FlexSensorFilter _flexsensor_filter[FLEXSENSOR_COUNT];

MPU6050 _mpu;
volatile bool _mpu_interrupt = false;
uint8_t _mpu_interrupt_status = 0;
uint16_t _mpu_packet_size = 0;
uint8_t _mpu_buffer[64];
uint16_t _mpu_buffer_available = 0;

uint8_t _mux_current = 0;

#ifdef SS_ENABLE_SLEEP
void power_interrupt() {
    power_wakeup();
}

void power_sleep() {
    mpu_cleanup();
    bluetooth_cleanup();
    multiplexer_cleanup();
    power_cleanup();

    attachInterrupt(digitalPinToInterrupt(BLUETOOTH_PIN_STATE), power_interrupt, RISING);
    set_sleep_mode(SLEEP_MODE_PWR_DOWN);
    sleep_enable();
    sleep_mode();
    sleep_disable();
}

void power_wakeup() {
    power_setup();
    multiplexer_setup();
    bluetooth_setup();
    mpu_setup();
}
#endif

void power_setup() {
    wdt_enable(WDTO_2S);

    power_spi_disable();
    power_timer1_disable();
    power_timer2_disable();

    pinMode(BATTERY_PIN_SENSE, INPUT);
}

int power_voltage() {
    return analogRead(BATTERY_PIN_SENSE);
}

void power_cleanup() { }

void power_reset() {
    while(true);
}

void multiplexer_setup() {
    pinMode(MULTIPLEXER_PIN_SIG, INPUT);
    pinMode(MULTIPLEXER_PIN_S3, OUTPUT);
    pinMode(MULTIPLEXER_PIN_S2, OUTPUT);
    pinMode(MULTIPLEXER_PIN_S1, OUTPUT);
    pinMode(MULTIPLEXER_PIN_S0, OUTPUT);
    pinMode(MULTIPLEXER_PIN_EN, OUTPUT);

    for(int i = 0; i < FLEXSENSOR_COUNT; ++i) _flexsensor_filter[i] = FlexSensorFilter();
}

void multiplexer_select(int c) {
    #ifdef MUX_HACK
        _mux_current = c;
    #endif

    digitalWrite(MULTIPLEXER_PIN_S0, bitRead(c, 0) ? HIGH : LOW);
    digitalWrite(MULTIPLEXER_PIN_S1, bitRead(c, 1) ? HIGH : LOW);
    digitalWrite(MULTIPLEXER_PIN_S2, bitRead(c, 2) ? HIGH : LOW);
    digitalWrite(MULTIPLEXER_PIN_S3, bitRead(c, 3) ? HIGH : LOW);
}

void multiplexer_enable() {
    digitalWrite(MULTIPLEXER_PIN_EN, LOW);
}

void multiplexer_disable() {
    digitalWrite(MULTIPLEXER_PIN_EN, HIGH);
}

int multiplexer_read(bool analog) {
    #ifdef MUX_HACK
    if(_mux_current == 8) {
        if(analog) return analogRead(A2); else return digitalRead(A2);
    } else if(_mux_current == 9) {
        if(analog) return analogRead(A3); else return digitalRead(A3);
    }
    #endif

    if(analog) return analogRead(MULTIPLEXER_PIN_SIG); else return digitalRead(MULTIPLEXER_PIN_SIG);
}

void multiplexer_write(bool analog, int value) {
    if(analog) analogWrite(MULTIPLEXER_PIN_SIG, value); else digitalWrite(MULTIPLEXER_PIN_SIG, value);
}

void multiplexer_cleanup() { }

void bluetooth_setup() {
    pinMode(BLUETOOTH_PIN_STATE, INPUT);
    pinMode(BLUETOOTH_PIN_EN, OUTPUT);

    _bluetooth.begin(BLUETOOTH_BAUD);
}

bool bluetooth_connected() {
    return true;
    return digitalRead(BLUETOOTH_PIN_STATE) == HIGH;
}

void bluetooth_send(uint8_t val) {
    _bluetooth.write(val);
    _bluetooth.flush();
}

void bluetooth_send(uint8_t* buf, size_t len) {
    _bluetooth.write(buf, len);
    _bluetooth.flush();
}

int bluetooth_available() {
    return _bluetooth.available();
}

int bluetooth_receive() {
    return _bluetooth.read();
}

void bluetooth_cleanup() {
    _bluetooth.end();
}

void mpu_interrupt() {
    _mpu_interrupt = true;
}

bool mpu_setup() {
    #if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
    Wire.begin();
    Wire.setClock(400000);
    #elif I2CDEV_IMPLEMENTATION == I2CDEV_BUILTIN_FASTWIRE
    Fastwire::setup(400, true);
    #endif

    _mpu.initialize();
    if(!_mpu.testConnection()) return false;

    int status = _mpu.dmpInitialize();
    if(status != 0) return false;

    _mpu.setXAccelOffset(-2927);
    _mpu.setYAccelOffset(-686);
    _mpu.setZAccelOffset(1333);
    _mpu.setXGyroOffset(37);
    _mpu.setYGyroOffset(-63);
    _mpu.setZGyroOffset(1);

    _mpu.setDMPEnabled(true);

    pinMode(MPU_PIN_INTERRUPT, INPUT);
    attachInterrupt(digitalPinToInterrupt(MPU_PIN_INTERRUPT), mpu_interrupt, RISING);

    _mpu_interrupt_status = _mpu.getIntStatus();
    _mpu_packet_size = _mpu.dmpGetFIFOPacketSize();

    return true;
}

void mpu_cleanup() { }

void setup() {
    power_setup();
    multiplexer_setup();
    bluetooth_setup();
    mpu_setup();

    Serial.begin(9600);
}

void loop() {
    if(bluetooth_connected()) {
        struct {
            uint16_t battery;
            uint16_t flex[FLEXSENSOR_COUNT];
            float rotation[3];
            uint16_t acceleration[3];
        } packet;

        if(bluetooth_available() > 0) {
            power_reset();
        }

        // Power
        packet.battery = power_voltage();

        // Flex
        for(int i = 0; i < FLEXSENSOR_COUNT; ++i) {
            multiplexer_select(i);
            _flexsensor_filter[i].put(multiplexer_read(true));
            _flexsensor_filter[i].compute();
            packet.flex[i] = _flexsensor_filter[i].get();
        }

        // MPU
        while(!_mpu_interrupt && (_mpu_buffer_available < _mpu_packet_size)) { }

        _mpu_interrupt = false;
        _mpu_interrupt_status = _mpu.getIntStatus();

        _mpu_buffer_available = _mpu.getFIFOCount();

        if((_mpu_interrupt_status & 0x10) || (_mpu_buffer_available == 1024)) {
            _mpu.resetFIFO();
        } else if(_mpu_interrupt_status & 0x02) {
            while(_mpu_buffer_available < _mpu_packet_size) _mpu_buffer_available = _mpu.getFIFOCount();

            _mpu.getFIFOBytes(_mpu_buffer, _mpu_packet_size);

            _mpu_buffer_available -= _mpu_packet_size;

            Quaternion quaternion;
            _mpu.dmpGetQuaternion(&quaternion, _mpu_buffer);

            VectorInt16 acceleration_raw;
            _mpu.dmpGetAccel(&acceleration_raw, _mpu_buffer);

            VectorFloat gravity;
            _mpu.dmpGetGravity(&gravity, &quaternion);

            _mpu.dmpGetYawPitchRoll(packet.rotation, &quaternion, &gravity);

            VectorInt16 acceleration_real;
            _mpu.dmpGetLinearAccel(&acceleration_real, &acceleration_raw, &gravity);

            VectorInt16 acceleration_world;
            _mpu.dmpGetLinearAccelInWorld(&acceleration_world, &acceleration_real, &quaternion);
            packet.acceleration[0] = acceleration_world.x;
            packet.acceleration[1] = acceleration_world.y;
            packet.acceleration[2] = acceleration_world.z;

            _mpu.resetFIFO();
        }

        static unsigned long last = millis();
        unsigned long now = millis();
        if(now - last > 50) {
            bluetooth_send(reinterpret_cast<uint8_t*>(&packet), sizeof(packet));
            bluetooth_send('\r');
            bluetooth_send('\n');
            last = now;
        }
    } else {
        #ifdef SS_ENABLE_SLEEP
        power_sleep();
        #else
        delay(1000);
        #endif
    }

    wdt_reset();
}
